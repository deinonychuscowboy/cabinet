#!/bin/bash

composer install

if ! [ -f app/config/parameters.yml ]; then
	composer run-script post-install-cmd
fi

rm -rf /var/www/.composer/cache/

modes=${MODE:-dev prod}
for mode in $modes; do
	bin/console cache:clear --env=$mode
	bin/console doctrine:schema:update --force
	if [ "dev" == "$mode" ]; then
		bin/console assets:install --env=$mode
	else
		bin/console assetic:dump --env=$mode
	fi
done
chown -R www-data ./var

keyfile=$(cat app/config/parameters.yml | grep "encryption_keyfile" | sed "s/.*: //")
if ! [ -f $keyfile ]; then
	echo
	echo "Generating a new keyfile at $keyfile"
	printf "\033[0;31mWARNING: Please back this key up; without it, your documents will be inaccessible!\033[0m"
	dd if=/dev/urandom bs=256 count=1 2>/dev/null | base64 | tr -d "\n" > $keyfile
fi
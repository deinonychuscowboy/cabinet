#!/bin/bash

set -xeuo pipefail

rm -f /tmp/dockerlog
mkfifo -m 777 /tmp/dockerlog
cat < /tmp/dockerlog 1>&2 &
php-fpm
lighttpd -D -f /etc/lighttpd/lighttpd.conf
ARG VARIANT
ARG MODE
ARG PHP_VERSION=7.0

FROM ubuntu:16.04 AS step-slim
ARG VARIANT
ARG MODE
ARG PHP_VERSION

ENV MODE=${MODE}
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
	apt-get -y dist-upgrade && \
	apt-get install -y php${PHP_VERSION}-fpm php${PHP_VERSION}-cli php${PHP_VERSION}-xml composer lighttpd zip unzip php-pdo-* && \
	if [ "${MODE}" = "dev" ]; then apt-get install -y php-xdebug; fi

COPY docker/lighttpd/lighttpd.conf /etc/lighttpd/lighttpd.conf
COPY docker/lighttpd/mimetype.conf /etc/lighttpd/mimetype.conf
COPY docker/lighttpd/${MODE}.conf /etc/lighttpd/environment.conf
COPY docker/php/php-fpm.conf /etc/php/${PHP_VERSION}/fpm/php-fpm.conf
COPY docker/php/${MODE}.ini /etc/php/${PHP_VERSION}/fpm/php.ini

WORKDIR /app/cabinet

FROM step-slim AS step-full
ARG VARIANT
ARG MODE
ARG PHP_VERSION

COPY . .

RUN composer install --no-scripts; \
	rm -rf /var/www/.composer/cache/

FROM step-${VARIANT} AS final
ARG VARIANT
ARG MODE
ARG PHP_VERSION

RUN rm -rf docker/ && \
	chmod u+s /usr/sbin/php-fpm${PHP_VERSION} && \
	ln -s /usr/sbin/php-fpm${PHP_VERSION} /usr/sbin/php-fpm && \
	mkdir -p /run/php/ && \
	chown www-data /run/php && \
	chown -R www-data /app && \
	chgrp -R www-data /app

CMD ["./start.bash"]
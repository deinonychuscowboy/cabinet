# Cabinet

Cabinet is a simple PHP applicaiton using Symfony 3 that acts as a digital filing cabinet. Its job is to store and retrieve PDF documents (no other filetypes are or will be supported). Documents are encrypted using phpseclib, so you can feel confident putting sensitive personal documents into the backing database as long as the application frontend itself is access-restricted (only accessible on localhost/secure network, physically secure, etc). There are no users or access controls presently, but these will probably be added eventually.

Docker
------

First time setup:

```
sudo git clean -xdf # erase any lingering deployment data from possible previous installs
sudo chown -R 33 var/ # give www-data user inside the container ownership of the var folder mounted as a volume
docker-compose build --no-cache app # build the image
docker-compose run --entrypoint ./setup.bash app # set up the symfony application
```

Regular startup:

```
docker-compose up app
```

The normal app target stores necessary persistent files on your host filesystem.

If you prefer to use the docker container to set up a non-docker application or want to keep your docker image smaller in size, you can use the app-external target, which will store everything in your host filesystem.

The app-dev target is also available for debugging purposes.
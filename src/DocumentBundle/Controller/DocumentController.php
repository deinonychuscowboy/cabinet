<?php

namespace DocumentBundle\Controller;

use DocumentBundle\Lib\ContainerAccess;
use DocumentBundle\Lib\Model\BinContent;
use DocumentBundle\Lib\Model\Document;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DocumentController extends Controller
{
	private function form($obj,$request,$allowUpload=true)
	{
		$form=$this->get("form.factory")->create(
			"DocumentBundle\\Lib\\Form\\Document",
			$obj,
			["upload"=>$allowUpload]
		);

		$form->handleRequest($request);

		if($form->isSubmitted()&&$form->isValid())
		{
			$this->get("doctrine")->getManager()->persist($obj);
			$this->get("doctrine")->getManager()->flush();

			return new RedirectResponse(
				$this->get("_router")->generate("display",["id"=>$obj->getId()])
			);
		}

		return [
			"form"=>$form->createView(),
		];
	}

	/**
	 * @Route("/new/", name="new_symfonyisdumb")
	 * @Route("/new", name="new")
	 * @Template()
	 */
	public function createAction(Request $request)
	{
		$obj=new Document();

		return $this->form($obj,$request);
	}

	/**
	 * @Route("/display/{id}/", name="display_symfonyisdumb")
	 * @Route("/display/{id}", name="display")
	 * @Template()
	 */
	public function readAction($id)
	{
		$objs=$this->get("doctrine")->getRepository("DocumentBundle\\Lib\\Model\\Document")->findBy(["id"=>$id]);

		if(count($objs)===0)
		{
			return new Response("No document with this id.",404);
		}

		return ["document"=>$objs[0]];
	}

	/**
	 * @Route("/open/{id}/", name="open_symfonyisdumb")
	 * @Route("/open/{id}", name="open")
	 */
	public function openAction($id)
	{
		$objs=$this->get("doctrine")->getRepository("DocumentBundle\\Lib\\Model\\Document")->findBy(["id"=>$id]);

		if(count($objs)===0)
		{
			return new Response("No document with this id.",404);
		}

		$response=new Response();
		$response->setContent(base64_decode($objs[0]->getContent()->getData()));
		$response->headers->set("Content-Disposition",["inline"]);
		$response->headers->set("Content-Type",["application/pdf"]);
		return $response;
	}

	/**
	 * @Route("/download/{id}/", name="download_symfonyisdumb")
	 * @Route("/download/{id}", name="download")
	 */
	public function downloadAction($id)
	{
		$objs=$this->get("doctrine")->getRepository("DocumentBundle\\Lib\\Model\\Document")->findBy(["id"=>$id]);

		if(count($objs)===0)
		{
			return new Response("No document with this id.",404);
		}

		$response=new Response();
		$response->setContent(base64_decode($objs[0]->getContent()->getData()));
		$response->headers->set("Content-Disposition",["attachment; filename=\"".preg_replace("/[^A-Za-z0-9_-]/","",$objs[0]->getTitle()).".pdf\""]);
		$response->headers->set("Content-Type",["application/pdf"]);
		return $response;
	}

	/**
	 * @Route("/", name="home")
	 * @Route("/list/", name="list_symfonyisdumb")
	 * @Route("/list", name="list")
	 * @Route("/list/with/{filter}/", name="list_with_symfonyisdumb")
	 * @Route("/list/with/{filter}", name="list_with")
	 * @Template()
	 */
	public function searchAction($filter=null)
	{
		$objects=$this->get("doctrine")->getRepository("DocumentBundle\\Lib\\Model\\Document")->findBy([],["timestamp"=>"DESC"]);

		if($filter!==null)
		{
			$objects=array_filter(
				$objects,
				function ($obj) use ($filter)
				{
					return in_array(
						$filter,
						array_map(
							function ($obj2)
							{
								return $obj2->getName();
							},
							$obj->getTags()
						)
					);
				}
			);
		}

		return ["documents"=>$objects];
	}

	public function setContainer(ContainerInterface $container=null)
	{
		parent::setContainer($container);
		ContainerAccess::set($this->container);
	}

	/**
	 * @Route("/edit/{id}/", name="edit_symfonyisdumb")
	 * @Route("/edit/{id}", name="edit")
	 * @Template()
	 */
	public function updateAction(Request $request,$id)
	{
		$obj=$this->get("doctrine")->getRepository("DocumentBundle\\Lib\\Model\\Document")->findBy(["id"=>$id]);

		return $this->form($obj[0],$request,false);
	}

	// no delete
}

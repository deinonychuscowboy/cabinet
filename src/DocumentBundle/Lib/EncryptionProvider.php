<?php

namespace DocumentBundle\Lib;

use Exception;
use phpseclib\Crypt\AES;
use phpseclib\Crypt\Base;
use phpseclib\Crypt\Blowfish;
use phpseclib\Crypt\DES;
use phpseclib\Crypt\Random;
use phpseclib\Crypt\RC2;
use phpseclib\Crypt\RC4;
use phpseclib\Crypt\Rijndael;
use phpseclib\Crypt\TripleDES;
use phpseclib\Crypt\Twofish;


class EncryptionProvider
{
	/**
	 * @var Base
	 */
	private $cipher;

	public function __construct()
	{
		$mode=Base::MODE_CBC;
		switch(ContainerAccess::getParameter("encryption_mode"))
		{
			case "CBC":
				break;
			case "CTR":
				$mode=Base::MODE_CTR;
				break;
			case "OFB":
				$mode=Base::MODE_OFB;
				break;
			case "CFB":
				$mode=Base::MODE_CFB;
				break;
			case "ECB":
				$mode=Base::MODE_ECB;
				break;
			case "STREAM":
				$mode=Base::MODE_STREAM;
				break;
			default:
				throw new Exception("Unknown encryption mode ".$mode);
				break;
		}
		switch(ContainerAccess::getParameter("encryption_alg"))
		{
			case "AES":
				$this->cipher=new AES($mode);
				break;
			case "DES":
				$this->cipher=new DES($mode);
				break;
			case "3DES":
				$this->cipher=new TripleDES($mode);
				break;
			case "Twofish":
				$this->cipher=new Twofish($mode);
				break;
			case "Blowfish":
				$this->cipher=new Blowfish($mode);
				break;
			case "RC2":
				$this->cipher=new RC2($mode);
				break;
			case "RC4":
				$this->cipher=new RC4($mode);
				break;
			case "Rijndael":
				$this->cipher=new Rijndael($mode);
				break;
			case "Plaintext":
				$this->cipher=null;
				break;
			default:
				throw new Exception("Unknown encryption algorithm ".ContainerAccess::getParameter("encryption_alg"));
				break;
		}
		$keyfile=ContainerAccess::getParameter("encryption_keyfile");
		if($keyfile[0]!=="/")
		{
			$keyfile=__DIR__."/../../../".$keyfile;
		}
		if(!is_file($keyfile))
		{
			throw new Exception($keyfile." is not the path of an encryption keyfile.");
		}
		$this->cipher->setKey(file_get_contents($keyfile));
	}

	public function decrypt($ciphertext)
	{
		if($this->cipher===null)
		{
			return $ciphertext;
		}
		else
		{
			$components=explode(";",$ciphertext);
			$this->cipher->setIV(base64_decode($components[0]));

			// rest of encrypted data may contain ;, so implode everything after the IV
			return $this->cipher->decrypt(implode(";",array_slice($components,1)));
		}
	}

	public function encrypt($plaintext)
	{
		if($this->cipher===null)
		{
			return $plaintext;
		}
		else
		{
			// create a new initialization vector for this encryption operation
			// shift right 3 to divide by 8 (block length is in bits, string provides bytes)
			$iv=Random::string($this->cipher->getBlockLength()>>3);
			$this->cipher->setIV($iv);

			// base64-encode the IV just to ensure it can't contain a ; delimiter, so we know the first ; is the delimiter
			return base64_encode($iv).";".$this->cipher->encrypt($plaintext);
		}
	}
}

<?php

namespace DocumentBundle\Lib\Form;

use DocumentBundle\Lib\Model\BinContent;
use DocumentBundle\Lib\Model\Tag;
use DomainException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Document extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder
			->add("title",TextType::class,["required"=>true])
			->add("description",TextareaType::class,["required"=>true]);
		if($options["upload"])
		{
			$builder->add("timestamp",DateType::class,["input"=>"timestamp","data"=>time(),"required"=>true]);
		}else{
			$builder->add("timestamp",DateType::class,["input"=>"timestamp","required"=>true]);
		}
			$builder->add("preview",ChoiceType::class,["choices"=>["Yes"=>true,"No"=>false],"expanded"=>"true","placeholder"=>false,"label"=>"Display Preview","required"=>false,"empty_data"=>false])
			->add("tags",EntityType::class,["class"=>Tag::class,"expanded"=>true,"multiple"=>true,"required"=>false]);
		if($options["upload"])
		{
			$builder->add("content",FileType::class,["required"=>true]);
			$builder
				->get("content")
		        ->addModelTransformer(
			        new CallbackTransformer(
				        function ($file)
				        {
					        // should never need to go this direction
					        return null;
				        },
				        function ($file)
				        {
					        if($file!==null&&get_class($file)!==UploadedFile::class)
					        {
						        throw new DomainException();
					        }

					        return $file===null
						        ?null
						        :new BinContent(base64_encode(file_get_contents($file->getRealPath())));
				        }
			        )
		        );
		}else{
			$builder->add("newcontent",FileType::class,["label"=>"New Content","required"=>false]);
			$builder
				->get("newcontent")
		        ->addModelTransformer(
			        new CallbackTransformer(
				        function ($file)
				        {
					        // should never need to go this direction
					        return null;
				        },
				        function ($file)
				        {
					        if($file!==null&&get_class($file)!==UploadedFile::class)
					        {
						        throw new DomainException();
					        }

					        return $file===null
						        ?null
						        :new BinContent(base64_encode(file_get_contents($file->getRealPath())));
				        }
			        )
		        );
		}
		$builder->add("submit",SubmitType::class);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			[
				"upload"=>true,
			]
		);
	}
}

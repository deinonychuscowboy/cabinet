<?php

namespace DocumentBundle\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ContainerAccess
{
	/**
	 * @var ContainerInterface
	 */
	private static $container;

	public static function get($key)
	{
		return self::$container->get($key);
	}

	public static function getParameter($key)
	{
		return self::$container->getParameter($key);
	}

	public static function set(ContainerInterface $container)
	{
		self::$container=$container;
	}
}

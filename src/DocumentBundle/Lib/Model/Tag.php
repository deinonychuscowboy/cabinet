<?php

namespace DocumentBundle\Lib\Model;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;

/**
 * @Entity
 * @Table("Tags")
 */
class Tag
{
	/**
	 * @Column(type="string",length=1024)
	 * @var string
	 */
	private $description;

	/**
	 * @ManyToMany(targetEntity="Document",mappedBy="tags")
	 * @var array
	 */
	private $documents;

	/**
	 * @Id
	 * @Column(type="string",length=32)
	 * @var string
	 */
	private $name;

	/**
	 * Create new instance of Tag.
	 *
	 * @param array  $documents
	 * @param string $name
	 * @param string $description
	 */
	public function __construct($documents,$name,$description)
	{
		$this->name=$name;
		$this->description=$description;

		$this->setDocuments($documents);
	}

	/**
	 * Get value of the Description.
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set value of the Description.
	 *
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description=$description;
	}

	/**
	 * Get value of the Documents.
	 *
	 * @return array
	 */
	public function getDocuments()
	{
		return $this->documents;
	}

	/**
	 * Set value of the Documents.
	 *
	 * @param array $documents
	 */
	public function setDocuments($documents)
	{
		if($documents!==null&&!is_array($documents))
		{
			$this->documents=$documents->getValues();
		}
		else
		{
			$this->documents=$documents;
		}
	}

	/**
	 * Get value of the Name.
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set value of the Name.
	 *
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name=$name;
	}
}

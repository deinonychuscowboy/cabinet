<?php

namespace DocumentBundle\Lib\Model;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

/**
 * @Entity
 * @Table("Documents")
 */
class Document
{
	/**
	 * @OneToOne(targetEntity="BinContent",inversedBy="document",cascade="persist")
	 * @var BinContent
	 */
	private $content;
	/**
	 * @Column(type="text")
	 * @var string
	 */
	private $description;
	/**
	 * @Id
	 * @GeneratedValue
	 * @Column(type="integer")
	 * @var int
	 */
	private $id;
	/**
	 * @ManyToMany(targetEntity="Tag",inversedBy="documents")
	 * @JoinTable(
	 *     name="RelTagsDocuments",
	 *     joinColumns={@JoinColumn(name="DocumentID", referencedColumnName="id")},
	 *     inverseJoinColumns={@JoinColumn(name="TagID", referencedColumnName="name")}
	 * )
	 * @var array
	 */
	private $tags;

	/**
	 * @Column(type="integer")
	 * @var int
	 */
	private $timestamp;
	/**
	 * @Column(type="string",length=256)
	 * @var string
	 */
	private $title;
	/**
	 * @Column(type="boolean",nullable=true)
	 * @var boolean
	 */
	private $preview;

	/**
	 * Create new instance of Document.
	 *
	 * @param string $title
	 * @param string $description
	 * @param array  $tags
	 * @param int    $timestamp
	 * @param string $content
	 */
	public function __construct($title=null,$description=null,$tags=[],$timestamp=null,$content=null)
	{
		$this->title=$title;
		$this->description=$description;
		$this->timestamp=$timestamp;
		$this->content=$content;

		$this->setTags($tags);
	}

	/**
	 * Get value of the Content.
	 *
	 * @return bool
	 */
	public function getPreview()
	{
		return $this->preview===null?true:$this->preview;
	}

	/**
	 * Set value of the Content.
	 *
	 * @param bool $preview
	 */
	public function setPreview($preview)
	{
		$this->preview=$preview;
	}

	/**
	 * Get value of the Content.
	 *
	 * @return BinContent
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * Set value of the Content.
	 *
	 * @param BinContent $contentId
	 */
	public function setContent(BinContent $content)
	{
		$this->content=$content;
	}

	public function getNewcontent(){
		return null;
	}

	public function setNewcontent($content){
		if(!empty($content)&&$content instanceof BinContent&&!empty($content->getRawData())){
			$this->setContent($content);
		}
	}

	/**
	 * Get value of the Description.
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set value of the Description.
	 *
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description=$description;
	}

	/**
	 * Get value of the Id.
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set value of the Id.
	 *
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id=$id;
	}

	/**
	 * Get value of the Tags.
	 *
	 * @return array
	 */
	public function getTags()
	{
		return $this->tags;
	}

	/**
	 * Set value of the Tags.
	 *
	 * @param array $tags
	 */
	public function setTags($tags)
	{
		if($tags!==null&&!is_array($tags))
		{
			$this->tags=$tags->getValues();
		}
		else
		{
			$this->tags=$tags;
		}
	}

	/**
	 * Get value of the Timestamp.
	 *
	 * @return int
	 */
	public function getTimestamp()
	{
		return $this->timestamp;
	}

	/**
	 * Set value of the Timestamp.
	 *
	 * @param int $timestamp
	 */
	public function setTimestamp($timestamp)
	{
		$this->timestamp=$timestamp;
	}

	/**
	 * Get value of the Title.
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set value of the Title.
	 *
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title=$title;
	}
}

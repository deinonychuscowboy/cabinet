<?php

namespace DocumentBundle\Lib\Model;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use DocumentBundle\Lib\ContainerAccess;

/**
 * @Entity
 * @Table("BinContent")
 *
 * Object that provides access to binary content in its raw plaintext form, but stores that content internally
 * as encrypted data and decrypts only when the data is requested.
 */
class BinContent
{
	/**
	 * @Column(type="blob")
	 * @var string
	 */
	private $data;
	/**
	 * @OneToOne(targetEntity="Document",mappedBy="content")
	 * @var Document
	 */
	private $document;
	/**
	 * @Id
	 * @GeneratedValue
	 * @Column(type="integer")
	 * @var int
	 */
	private $id;

	/**
	 * Create new instance of BinContent.
	 *
	 * @param string $data plaintext data
	 */
	public function __construct($data=null)
	{
		if($data!==null)
		{
			$this->setData($data);
		}
	}

	/**
	 * Get value of the Content.
	 *
	 * @return mixed
	 */
	public function getData()
	{
		// Doctrine provides binary types as a resource; read these into normal memory transparently
		if(is_resource($this->data))
		{
			$this->data=stream_get_contents($this->data);
		}

		return ContainerAccess::get("encryptor")->decrypt($this->data);
	}

	/**
	 * Set value of the Content.
	 *
	 * @param mixed $data
	 */
	public function setData($data)
	{
		$this->data=ContainerAccess::get("encryptor")->encrypt($data);
	}

	public function getRawData(){
		return $this->data;
	}

	public function setRawData($data){
		$this->data=$data;
	}

	/**
	 * Get value of the Owner.
	 *
	 * @return int
	 */
	public function getDocument()
	{
		return $this->document;
	}

	/**
	 * Set value of the Owner.
	 *
	 * @param int $document
	 */
	public function setDocument($document)
	{
		$this->document=$document;
	}

	/**
	 * Get value of the Id.
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set value of the Id.
	 *
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id=$id;
	}
}
